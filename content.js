comments = null
$(document).ready(function () {
    console.log("Scraper is ready!");
    var overlay = $("#collegedate-scraper");
    if (overlay.length == 0) {
        overlay = $("<div id='collegedate-scraper'><button type='button' id='download-btn' style='margin:5px'>Download</button></div>");
        overlay.css({
            "position": "fixed",
            "width": "120px",
            "height": "40px",
            "z-index": "99999",
            "top": "20px",
            "left": "20px",
            "background-color": "RGB(71, 114, 158)",
            "cursor": "pointer",
            "-webkit-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-moz-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-ms-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "-o-transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)",
            "transition": "all 1000ms cubic-bezier(0.57, 0.06, 0.05, 0.95)"
        });
    }
    $("body").append(overlay);

    var button = $("#download-btn");
    button.click(function (event) {
        event.preventDefault();
            scrapData(true);
    });


    var full_data_dict = {};

    async function scrapData(get_raw)  {
      const result = [];
      comments = $("div.comments");
      if (comments.length == 0) {
        alert("No Comment");
        return;
      }

      await scrollToBottom();

      const commentItems = Array.from(comments[0].children);
      if (commentItems.length == 0) {
        alert("No Comment");
        return;
      }
      commentItems.forEach(comment => {
        const link=$("a.user-info",comment)[0].href
        const userName = $("span.username",comment)[0].textContent
        const commentText = $("p.comment-text > span",comment)[0].textContent.replace(/,/g,".");
        const phoneMatch = commentText.match(/0[\d,-]{6,15}/);
        phone = phoneMatch == null ? "" : phoneMatch[0];

        result.push([link, userName, commentText, phone]);
      });
      save_to_file(result);

    }

    function save_to_file(result) {
      keys = ["link","userName","commentText","phone"]
        var string_to_write = keys.join(",") + '\n';
        for (arr of result) {
          string_to_write += arr.join(",") + '\n';
        }
      console.log(string_to_write);

      var blob = new Blob([string_to_write], { // Creates the file
          type: "text/plain;charset=utf-8"
      });
      saveAs(blob, "data.csv"); // Supposedly save's it (this is the problem)
      console.log("Scraping finished!");
    }

});

   async function scrollToBottom() {
      const commentDiv = $("div.comment-container")[0];
      let curheight = 0;
      while(curheight < commentDiv.scrollHeight) {
        curheight = commentDiv.scrollHeight;
        commentDiv.scrollTop = commentDiv.scrollHeight;
        console.log(`commentDiv.scrollHeight = ${commentDiv.scrollHeight}`);
        await sleep(200);
      }

}
const sleep = (msec) => new Promise((resolve) => setTimeout(resolve, msec));
